public class FatorialRecursivo{
   public statis int calcularFatorialRecursivo(int n) throws IllegalArgumentException{
       if (n == 0)
              return 1;
       return n * calcularFatorialRecursivo(n-1)
   }
   public statis void main(String[] args){
	System.out.println(calcularFatorialRecursivo(5));   
}
}